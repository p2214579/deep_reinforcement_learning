from DQNAgent import DQNAgent
import gym
import random
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import matplotlib.pylab as plt

if __name__ == '__main__':
	agent = DQNAgent(4, 2)
	env = gym.make("CartPole-v1")
	state = env.reset()
	action = random.randint(0, 1)
	vid = VideoRecorder(env, path='video.mp4')
	
	interact_nbr = 0
	interactions = {}
	episode_rewards = {}

	for episode in range(50):
		observation = env.reset()
		
		total_reward = 0

		for t in range(100):
			interact_nbr +=1
			env.render()
			vid.capture_frame()

			action = env.action_space.sample()
			observation, reward, done, info = env.step(action)

			if episode not in episode_rewards.keys():
				episode_rewards[episode] = reward
			else:
				episode_rewards[episode] += reward

			if done:
				interactions[interact_nbr] = episode_rewards[episode]
				break

	vid.close()
	vid.enabled = False		

	x, y = zip(*sorted(interactions.items()))	

	plt.xlabel('nbr of interactions')
	plt.ylabel('total reward')
	plt.title('R(nbr_interactions)')

	plt.plot(x, y)
	plt.show()		







 


	