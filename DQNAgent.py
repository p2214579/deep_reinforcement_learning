import random
import gym
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
import os # for creating directories
import matplotlib.pylab as plt
from gym.wrappers.monitoring.video_recorder import VideoRecorder

batch_size = 32
n_episodes = 1001 # n games we want agent to play (default 1001)
output_dir = 'model_output/cartpole/'
state_size = 4
action_size = 2

class Buffer:
	def __init__(self, len):
		self.buffer = deque(maxlen=len)

	def add_exp(self,state, action, reward, next_state, done):
		butter.append(state, action, reward, next_state, done)
		

	def getBatch(self, batch_size=32):
		batch = random.sample(self.buffer, batch_size)
		return batch



class  DQNAgent:

	def __init__(self, state_size, action_size):
		
		self.state_size = state_size
		self.action_size = action_size

		self.memory = Buffer(200)

		self.gamma = 0.95

		self.epsilon = 1.0
		self.epsilon_decay = 0.995
		self.epsilon_min = 0.001
		
		self.learning_rate = 0.001
		self.model = self._build_model()

	def _build_model(self):
		model = Sequential()
		model.add(Dense(24, input_dim=self.state_size, activation='relu'))
		model.add(Dense(24, activation='relu'))
		model.add(Dense(self.action_size, activation='linear'))

		model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate))

		return model

	def store(self, state, action, reward, next_state, done):
		self.memory.add_exp(state, action, reward, next_state, done)

	def get_action(self, state, exp_type=True):

		# EXPLORATION EPSILON GREEDY 
		if exp_type :
			if np.random.rand() <= self.epsilon:
				#return random.randrange(self.action_size)
				pass
				print(state)
			act_values = self.model.predict(state)
			return np.argmax(act_values[0])

		# ACT RANDOMLY WITHOUT EXPLORATION	
		else:
			return random.randint(0, 1)

	def getPredictAction(self,state):
		state = np.reshape(state, (1, 4))
		act_values = self.model.predict(state)
		return np.argmax(act_values[0])


	def learn(self, batch_size):
		minibatch = memory.getBatch(batch_size)
		for state, action, reward, next_state, done in minibatch:
			target = reward
			if not done:
				target = (reward + self.gamma * np.amax(self.model.predict(next_state)[0]))
			target_f = self.model.predict(state)
			target_f[0][action] = target
			self.model.fit(state, target_f, epochs=1, verbose=0)

			if self.epsilon > self.epsilon_min:
				self.epsilon *= self.epsilon_decay

	def load(self, name):
		self.model.load_weights(name)


	def save(self, name):
		self.model.save_weights(name)


def train():
	done = False
	for e in range(n_episodes):
	    state = env.reset() 
	    state = np.reshape(state, (1, state_size))
	    
	    for time in range(5000): 
	        #env.render()
	        action = agent.get_action(state)
	        next_state, reward, done, _ = env.step(action)  
	        reward = reward if not done else -10         
	        next_state = np.reshape(next_state, [1, state_size])
	        agent.store(state, action, reward, next_state, done)        
	        state = next_state      
	        if done: 
	            print("episode: {}/{}, score: {}, e: {:.2}" 
	                  .format(e, n_episodes, time, agent.epsilon))
	            break # exit loop
	    if len(agent.memory.buffer) > batch_size:
	        agent.replay(batch_size) 
	    if e % 50 == 0:
	        agent.save(output_dir + "weights_" + '{:04d}'.format(e) + ".hdf5")  

def agent_interact_env(agent, env, save=True, file='video.mp4', nbr_episode=20):

	interact_nbr = 0
	interactions = {}
	episode_rewards = {}
	action = random.randint(0, 1)

	if save:
		vid = VideoRecorder(env, path=file)

	for episode in range(nbr_episode):
		observation = env.reset()
		
		total_reward = 0

		for t in range(100):
			interact_nbr +=1
			env.render()

			if save:
				vid.capture_frame()

			
			observation, reward, done, info = env.step(action)
			action = agent.getPredictAction(observation)

			if episode not in episode_rewards.keys():
				episode_rewards[episode] = reward
			else: 
				episode_rewards[episode] += reward

			if done:
				interactions[interact_nbr] = episode_rewards[episode]
				break

	if save:			
		vid.close()
		vid.enabled = False		

	x, y = zip(*sorted(episode_rewards.items()))	

	plt.xlabel('nbr episode')
	plt.ylabel('total reward')
	plt.title('R(episode)')

	plt.plot(x, y)
	plt.show()	



if __name__ == '__main__':

	agent = DQNAgent(4, 2)
	agent.load('weights_1000.hdf5')
	env = gym.make("CartPole-v1")
	state = env.reset()

	agent_interact_env(agent, env)